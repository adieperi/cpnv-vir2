# cpnv-vir2

## Prerequisites
### Install Proxmox LXC template
![proxmox](./assets/img/proxmox.png)

### Install git an clone repo
```bash
apt install git
git clone https://gitlab.com/adieperi/cpnv-vir2.git
cd cpnv-vir2
```

### Change template name if is needed
```bash
# TEMPLATE_NAME='debian-11-standard_11.7-1_amd64.tar.zst'
nano .env
```

## Create all templates
```bash
./pentester create-templates
```

## Create pentester template
```bash
./pentester create-pentester-template
```

## Create attacker template
```bash
./pentester create-attacker-template
```

## Run infrastructure
Change the number if you want more
```bash
./pentester student 2
```

## Stop infrastructure
```bash
./pentester stop
```

## Start infrastructure
```bash
./pentester start
```

## List containers
```bash
./pentester list
```

## Add new softwares in pentester-template
```bash
nano ./ansible/vars/pentester.yml
./pentester create-pentester-template
```

## Add new softwares in attacker-template
```bash
nano ./ansible/vars/attacker.yml
./pentester create-attacker-template
```

## Containers root credential
Username : **root**\
Password : **root1234**
